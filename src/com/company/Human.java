package com.company;

import java.util.Arrays;

public class Human {
    private Pet pet;
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Human mother;
    private Human father;
    private String[][] schedule = {
            {"Sunday", "do home work"},
            {"Monday", "go to courses; watch a film"},
            {"Tuesday", "eat ice-cream"},
            {"Wednesday", "go to shopping"},
            {"Thursday", "talk with friends"},
            {"Friday", "buy present for mom"},
            {"Saturday", "listen to the music"}};


    public Human(Pet pet, String name, String surname, int year, int iq, Human mother, Human father, String[][] schedule) {
        this.pet = pet;
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.mother = mother;
        this.father = father;
        this.schedule = schedule;
    }

    public Human() {
    }

    public String petNicknameAndHumanName(){
        return this.pet.getNickname() + " " + this.name;
    }

    @Override
    public String toString() {
        return "Human{" +
                "pet=" + pet +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", mother=" + mother +
                ", father=" + father +
                '}';
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }
    //-------
    public void greetPet () {
        System.out.println("Hello, " + this.pet.getNickname());
    }
    public void describePet () {
        System.out.println("I have a " + this.pet.getSpecies() + ", he is " + this.pet.getAge() + " years old, he is " + ((this.pet.getTrickLevel() < 50) ? "almost not slowly" : "very slowly" ));
    }


}
